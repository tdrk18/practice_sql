USE practice;

INSERT INTO Employee VALUES('E00001', '山田太郎', '男', '1995-01-01', NULL, NULL);
INSERT INTO Employee VALUES('E00002', '佐藤次郎', '男', '1990-05-03', 250000, 'D001');
INSERT INTO Employee VALUES('E00003', '鈴木花子', '女', '1990-02-11', 250000, 'D002');
INSERT INTO Employee VALUES('E00004', '田中三郎', '男', '1975-11-03', 400000, 'D003');
INSERT INTO Employee VALUES('E00005', '高橋良子', '女', '1985-11-23', 300000, 'D003');
INSERT INTO Employee VALUES('E00006', '鈴木良枝', '女', '1970-05-03', 450000, 'D003');
INSERT INTO Employee VALUES('E00007', '佐藤健次', '男', '1980-05-05', 350000, 'D001');

INSERT INTO Department VALUES('D001', '総務部', 8);
INSERT INTO Department VALUES('D002', '人材開発部', 8);
INSERT INTO Department VALUES('D003', 'システム開発部', 12);
