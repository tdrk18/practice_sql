DROP DATABASE IF EXISTS practice;
CREATE DATABASE practice;
USE practice;

DROP TABLE IF EXISTS `Employee`;
CREATE TABLE `practice`.`Employee` (
  `emp_id` CHAR(6) NOT NULL,
  `name` VARCHAR(10) NOT NULL,
  `gender` CHAR(1) NOT NULL,
  `birthday` DATE NOT NULL,
  `salary` INT NULL,
  `dep_id` CHAR(4) NULL,
  PRIMARY KEY (`emp_id`)
);

DROP TABLE IF EXISTS `Department`;
CREATE TABLE `practice`.`Department` (
  `dep_id` CHAR(4) NOT NULL,
  `name` VARCHAR(10) NOT NULL,
  `floor` INT NULL,
  PRIMARY KEY (`dep_id`)
);
